#!/bin/bash

echo "Removing all but Git items."

find . ! -path '*git*' -exec rm -rf {} +
